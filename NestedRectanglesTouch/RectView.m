//
//  RectView.m
//  NestedRectanglesTouch
//
//  Created by Pavel Smirnov on 3/29/16.
//  Copyright © 2016 Pavel Smirnov. All rights reserved.
//

#import "RectView.h"

@interface RectView ()

@property (strong, nonatomic) UITouch *currentTouch;
@property (nonatomic) CGPoint startTouchPosition;

@property (nonatomic) BOOL longTouchStarted;

@end

@implementation RectView

#pragma mark - Touches handling

#define SWIPE_MIN 10

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{    
    NSSet *allTouches = [event allTouches];
    UITouch *touch = [[allTouches allObjects] objectAtIndex:0];
    
    [self setCurrentTouch:[touches anyObject]];
    [self setStartTouchPosition:[touch locationInView:[[touches anyObject] view]]];
    
    [self performSelector:@selector(recognizeLongTap)
               withObject:self
               afterDelay:0.5f];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSSet *allTouches = [event allTouches];
    UITouch *touch = [[allTouches allObjects] objectAtIndex:0];
    CGPoint touchLocation = [touch locationInView:[[touches anyObject] view]];
    
    if (fabs(self.startTouchPosition.x - touchLocation.x) >= SWIPE_MIN ||
        fabs(self.startTouchPosition.y - touchLocation.y) >= SWIPE_MIN)
    {
        [[self delegate] rectangleDidReceiveSwipe:self];
    }
    
    NSLog(@"%@", NSStringFromCGPoint(touchLocation));
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touch ended");
    
    if (![self longTouchStarted])
    {
        [[self delegate] rectangleDidReceiveTap:self];
    }
    else
    {
        [[self delegate] rectangleDidFinishLongPress:self];
    }
    
    [self setLongTouchStarted:NO];
}

- (void)recognizeLongTap
{
    NSLog(@"Long tap is recognizing");
    
    if ([[self currentTouch] phase] == UITouchPhaseStationary)
    {
        [self setLongTouchStarted:YES];
        [[self delegate] rectangleDidReceiveLongTap:self];
    }
}

@end
