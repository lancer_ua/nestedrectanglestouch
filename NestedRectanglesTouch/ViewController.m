//
//  ViewController.m
//  NestedRectanglesTouch
//
//  Created by Pavel Smirnov on 3/28/16.
//  Copyright © 2016 Pavel Smirnov. All rights reserved.
//

#import "ViewController.h"
#import "RectView.h"

@interface ViewController () <ViewDelegate>

@property (nonatomic, readwrite, assign) NSMutableArray* rectanglesArray;
@property (assign, nonatomic) RectView* chameleonView;

@property (strong, nonatomic) CADisplayLink *longTapActionTimer;
@property (nonatomic) CFTimeInterval initialTimestamp;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    RectView* baseView = [[RectView alloc] initWithFrame:[[self view] bounds]];
    
    [[self view] addSubview:baseView];
    
    [self buildNestedRectanglesOnView:baseView];
}

#pragma mark - Sequential rectangles creation

- (void)buildNestedRectanglesOnView:(RectView*)rootView
{
    RectView* baseView = rootView;
    baseView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    float spacing = [self spacingRectanglesInView:baseView];
    
    [self setRectanglesArray:[[NSMutableArray alloc] init]];
    [self.rectanglesArray addObject:baseView];
    
    CGFloat hue = 0;
    
    RectView* parentRect = baseView;
    
    for (NSInteger i = 0; i < [self rectanglesQuantity]; i++)
    {
        RectView* subRect = [[RectView alloc]
                           initWithFrame:CGRectMake((parentRect.bounds.origin.x + spacing),
                                                    (parentRect.bounds.origin.y + spacing),
                                                    (parentRect.bounds.size.width - (spacing * 2)),
                                                    (parentRect.bounds.size.height - (spacing * 2)))];
        
        [subRect setBackgroundColor:[UIColor colorWithHue:hue+=((1.0 / 360.0) * (350.0 / [self rectanglesQuantity])) saturation:0.9 brightness:0.9 alpha:1.0]];
        subRect.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [subRect setViewHue:hue];
        [subRect setDelegate:self];
        [parentRect addSubview:subRect];
        [self.rectanglesArray addObject:subRect];
        
        parentRect = subRect;
        
        [subRect release];
    }
    
}

#pragma mark - Methods for subviews

- (CGFloat)spacingRectanglesInView:(UIView *)myView
{
    CGFloat minRectangleX = (myView.center.x / 2);
    CGFloat minRectangleY = (myView.center.y / 2);
    
    //     Calculating length of vector from (0;0) to half of center
    NSInteger minMaxRectangleSpace = minRectangleX + minRectangleY;
    
    CGFloat spacing = ((minMaxRectangleSpace / 2) / ([self rectanglesQuantity]));
    
    return spacing;
}

- (NSInteger)rectanglesQuantity
{
    return 10;
}

#pragma mark - Coloring

- (UIColor*)pickRandomColor
{
    CGFloat redLevel    = arc4random() % 256 / 255.0;
    CGFloat greenLevel  = arc4random() % 256 / 255.0;
    CGFloat blueLevel   = arc4random() % 256 / 255.0;
    
    UIColor *color = [UIColor colorWithRed:redLevel green:greenLevel blue:blueLevel alpha:1.0];
    
    return color;
}

- (void)startChameleoningForView:(RectView *)myView
{
    [self setChameleonView:myView];
    
    CADisplayLink *dispLink = [CADisplayLink displayLinkWithTarget:self
                                                          selector:@selector(linearChameleoning)];
        
    [dispLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    [self setLongTapActionTimer:dispLink];
    [self setInitialTimestamp:CACurrentMediaTime()];
}

- (void)linearChameleoning
{
    UIColor *backGround = [self colorForLinearChameleonTick];
    [[self chameleonView] setBackgroundColor:backGround];
    NSLog(@"Long tap with hue = %f", [[self chameleonView] viewHue]);
}

- (UIColor *)colorForLinearChameleonTick
{
    if ([[self chameleonView] viewHue] > 0.999f)
    {
        [[self chameleonView] setViewHue:0.f];
    }
    return [UIColor colorWithHue:[self chameleonView].viewHue+=(1.0 / 360.0) saturation:0.9 brightness:0.9 alpha:1.0];
}

- (void)finishChameleoning
{
    [[self longTapActionTimer] removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    [[self longTapActionTimer] invalidate];
}

#pragma mark - ViewDelegate

- (void)rectangleDidReceiveTap:(RectView *) viewRect
{
    NSLog(@"rect received tap");
    for (RectView *myRectangle in [self rectanglesArray])
    {
        if ([myRectangle isEqual:viewRect])
        {
            [viewRect setBackgroundColor:[self pickRandomColor]];
            [self setChameleonView:nil];
        }
    }
}

- (void)rectangleDidReceiveLongTap:(RectView *) viewRect
{
    [self startChameleoningForView:viewRect];
}

- (void)rectangleDidReceiveSwipe:(RectView *) viewRect
{
    for (RectView *myRectangle in [self rectanglesArray])
    {
        if ([myRectangle isEqual:viewRect])
        {
            [viewRect removeFromSuperview];
        }
    }
}

- (void)rectangleDidFinishLongPress:(RectView *) viewRect
{
    [self finishChameleoning];
}

@end

