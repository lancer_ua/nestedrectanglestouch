//
//  RectView.h
//  NestedRectanglesTouch
//
//  Created by Pavel Smirnov on 3/29/16.
//  Copyright © 2016 Pavel Smirnov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ViewDelegate;

@interface RectView : UIView

@property (assign, nonatomic) id <ViewDelegate> delegate;
@property (nonatomic) CGFloat viewHue;

@end

@protocol ViewDelegate <NSObject>

- (void)rectangleDidReceiveTap:(RectView *) viewRect;
- (void)rectangleDidReceiveLongTap:(RectView *) viewRect;
- (void)rectangleDidReceiveSwipe:(RectView *) viewRect;
- (void)rectangleDidFinishLongPress:(RectView *) viewRect;

@end
